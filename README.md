# NG Fitness application

This is a demo project wich using Angular Material Design, NgRx reducer for state management, module lazy loading, Angularfire and Firebase for backend and backend communication.

## Demo

A demo of this application could be find at `https://ng-fitness-tracker-506b6.web.app`.

## Configuration
If you want to build this application, don't forget to add your personal firebase environment keys in `environments/environment.ts` (and also) in `environments/environment.prod.ts`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

